﻿using System;
using System.Drawing;
using System.ComponentModel;

namespace ColorOverlayCs
{
    public class ColorModel : INotifyPropertyChanged
    {
        private static string[] COLOR_PROPERTIES = new string[]
        {
                "AlphaString",
                "RedString",
                "GreenString",
                "BlueString",
                "AlphaValue",
                "RedValue",
                "GreenValue",
                "BlueValue",
        };
        private Color color = Color.FromArgb(0, 0, 0);
        public event PropertyChangedEventHandler PropertyChanged;
        public Color Color
        {
            get => color;
            set
            {
                color = value;
                if (PropertyChanged != null)
                {
                    foreach (string propertyName in COLOR_PROPERTIES)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                    }
                }
            }
        }
        public string AlphaString
        {
            get => color.A.ToString();
            set
            {
                byte v;
                if (byte.TryParse(value, out v))
                {
                    AlphaValue = v;
                }
            }
        }
        public string RedString
        {
            get => color.R.ToString();
            set
            {
                byte v;
                if (byte.TryParse(value, out v))
                {
                    RedValue = v;
                }
            }
        }
        public string GreenString
        {
            get => color.G.ToString();
            set
            {
                byte v;
                if (byte.TryParse(value, out v))
                {
                    GreenValue = v;
                }
            }
        }
        public string BlueString
        {
            get => color.B.ToString();
            set
            {
                byte v;
                if (byte.TryParse(value, out v))
                {
                    BlueValue = v;
                }
            }
        }
        public byte AlphaValue
        {
            get => color.A;
            set => color = Color.FromArgb(value, color.R, color.G, color.B);
        }
        public byte RedValue
        {
            get => color.R;
            set => color = Color.FromArgb(color.A, value, color.G, color.B);
        }
        public byte GreenValue
        {
            get => color.G;
            set => color = Color.FromArgb(color.A, color.R, value, color.B);
        }
        public byte BlueValue
        {
            get => color.B;
            set => color = Color.FromArgb(color.A, color.R, color.G, value);
        }
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
