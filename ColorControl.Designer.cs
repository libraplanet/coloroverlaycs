﻿namespace ColorOverlayCs
{
    partial class ColorControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.panelCur = new System.Windows.Forms.Panel();
            this.textBoxBlue = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxGreen = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxRed = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAlpha = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel09 = new System.Windows.Forms.Panel();
            this.panel08 = new System.Windows.Forms.Panel();
            this.panel07 = new System.Windows.Forms.Panel();
            this.panel06 = new System.Windows.Forms.Panel();
            this.panel05 = new System.Windows.Forms.Panel();
            this.panel04 = new System.Windows.Forms.Panel();
            this.panel03 = new System.Windows.Forms.Panel();
            this.panel02 = new System.Windows.Forms.Panel();
            this.panel01 = new System.Windows.Forms.Panel();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // panelCur
            // 
            this.panelCur.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelCur.Location = new System.Drawing.Point(283, 3);
            this.panelCur.Name = "panelCur";
            this.panelCur.Size = new System.Drawing.Size(84, 84);
            this.panelCur.TabIndex = 26;
            this.panelCur.TabStop = true;
            this.panelCur.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelCur_MouseClick);
            this.panelCur.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panelCur_MouseDoubleClick);
            // 
            // textBoxBlue
            // 
            this.textBoxBlue.Location = new System.Drawing.Point(230, 66);
            this.textBoxBlue.MaxLength = 3;
            this.textBoxBlue.Name = "textBoxBlue";
            this.textBoxBlue.Size = new System.Drawing.Size(47, 19);
            this.textBoxBlue.TabIndex = 25;
            this.textBoxBlue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(189, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 12);
            this.label4.TabIndex = 24;
            this.label4.Text = "blue";
            // 
            // textBoxGreen
            // 
            this.textBoxGreen.Location = new System.Drawing.Point(230, 45);
            this.textBoxGreen.MaxLength = 3;
            this.textBoxGreen.Name = "textBoxGreen";
            this.textBoxGreen.Size = new System.Drawing.Size(47, 19);
            this.textBoxGreen.TabIndex = 23;
            this.textBoxGreen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(189, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 12);
            this.label3.TabIndex = 22;
            this.label3.Text = "green";
            // 
            // textBoxRed
            // 
            this.textBoxRed.Location = new System.Drawing.Point(230, 24);
            this.textBoxRed.MaxLength = 3;
            this.textBoxRed.Name = "textBoxRed";
            this.textBoxRed.Size = new System.Drawing.Size(47, 19);
            this.textBoxRed.TabIndex = 21;
            this.textBoxRed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(189, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 12);
            this.label2.TabIndex = 20;
            this.label2.Text = "red";
            // 
            // textBoxAlpha
            // 
            this.textBoxAlpha.Location = new System.Drawing.Point(230, 3);
            this.textBoxAlpha.MaxLength = 3;
            this.textBoxAlpha.Name = "textBoxAlpha";
            this.textBoxAlpha.Size = new System.Drawing.Size(47, 19);
            this.textBoxAlpha.TabIndex = 19;
            this.textBoxAlpha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(189, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 12);
            this.label1.TabIndex = 18;
            this.label1.Text = "alpha";
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel18.Location = new System.Drawing.Point(153, 63);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(24, 24);
            this.panel18.TabIndex = 17;
            this.panel18.TabStop = true;
            this.panel18.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel18.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel17.Location = new System.Drawing.Point(123, 63);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(24, 24);
            this.panel17.TabIndex = 16;
            this.panel17.TabStop = true;
            this.panel17.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel17.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel16
            // 
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel16.Location = new System.Drawing.Point(93, 63);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(24, 24);
            this.panel16.TabIndex = 15;
            this.panel16.TabStop = true;
            this.panel16.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel16.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel15.Location = new System.Drawing.Point(63, 63);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(24, 24);
            this.panel15.TabIndex = 14;
            this.panel15.TabStop = true;
            this.panel15.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel15.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel14.Location = new System.Drawing.Point(33, 63);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(24, 24);
            this.panel14.TabIndex = 13;
            this.panel14.TabStop = true;
            this.panel14.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel14.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel13.Location = new System.Drawing.Point(3, 63);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(24, 24);
            this.panel13.TabIndex = 12;
            this.panel13.TabStop = true;
            this.panel13.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel13.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel12.Location = new System.Drawing.Point(153, 33);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(24, 24);
            this.panel12.TabIndex = 11;
            this.panel12.TabStop = true;
            this.panel12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel12.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Location = new System.Drawing.Point(123, 33);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(24, 24);
            this.panel11.TabIndex = 10;
            this.panel11.TabStop = true;
            this.panel11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel11.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Location = new System.Drawing.Point(93, 33);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(24, 24);
            this.panel10.TabIndex = 9;
            this.panel10.TabStop = true;
            this.panel10.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel10.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel09
            // 
            this.panel09.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel09.Location = new System.Drawing.Point(63, 33);
            this.panel09.Name = "panel09";
            this.panel09.Size = new System.Drawing.Size(24, 24);
            this.panel09.TabIndex = 8;
            this.panel09.TabStop = true;
            this.panel09.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel09.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel08
            // 
            this.panel08.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel08.Location = new System.Drawing.Point(33, 33);
            this.panel08.Name = "panel08";
            this.panel08.Size = new System.Drawing.Size(24, 24);
            this.panel08.TabIndex = 7;
            this.panel08.TabStop = true;
            this.panel08.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel08.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel07
            // 
            this.panel07.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel07.Location = new System.Drawing.Point(3, 33);
            this.panel07.Name = "panel07";
            this.panel07.Size = new System.Drawing.Size(24, 24);
            this.panel07.TabIndex = 6;
            this.panel07.TabStop = true;
            this.panel07.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel07.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel06
            // 
            this.panel06.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel06.Location = new System.Drawing.Point(153, 3);
            this.panel06.Name = "panel06";
            this.panel06.Size = new System.Drawing.Size(24, 24);
            this.panel06.TabIndex = 5;
            this.panel06.TabStop = true;
            this.panel06.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel06.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel05
            // 
            this.panel05.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel05.Location = new System.Drawing.Point(123, 3);
            this.panel05.Name = "panel05";
            this.panel05.Size = new System.Drawing.Size(24, 24);
            this.panel05.TabIndex = 4;
            this.panel05.TabStop = true;
            this.panel05.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel05.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel04
            // 
            this.panel04.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel04.Location = new System.Drawing.Point(93, 3);
            this.panel04.Name = "panel04";
            this.panel04.Size = new System.Drawing.Size(24, 24);
            this.panel04.TabIndex = 3;
            this.panel04.TabStop = true;
            this.panel04.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel04.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel03
            // 
            this.panel03.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel03.Location = new System.Drawing.Point(63, 3);
            this.panel03.Name = "panel03";
            this.panel03.Size = new System.Drawing.Size(24, 24);
            this.panel03.TabIndex = 2;
            this.panel03.TabStop = true;
            this.panel03.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel03.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel02
            // 
            this.panel02.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel02.Location = new System.Drawing.Point(33, 3);
            this.panel02.Name = "panel02";
            this.panel02.Size = new System.Drawing.Size(24, 24);
            this.panel02.TabIndex = 1;
            this.panel02.TabStop = true;
            this.panel02.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel02.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // panel01
            // 
            this.panel01.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel01.Location = new System.Drawing.Point(3, 3);
            this.panel01.Name = "panel01";
            this.panel01.Size = new System.Drawing.Size(24, 24);
            this.panel01.TabIndex = 0;
            this.panel01.TabStop = true;
            this.panel01.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.panel01.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDoubleClick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "|画像ファイル|*.bmp,*.jpg,*.png,*.gif|すべてのファイル|*.*|";
            // 
            // ColorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelCur);
            this.Controls.Add(this.textBoxBlue);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel01);
            this.Controls.Add(this.textBoxGreen);
            this.Controls.Add(this.panel02);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel03);
            this.Controls.Add(this.textBoxRed);
            this.Controls.Add(this.panel04);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel05);
            this.Controls.Add(this.textBoxAlpha);
            this.Controls.Add(this.panel06);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel07);
            this.Controls.Add(this.panel18);
            this.Controls.Add(this.panel08);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel09);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel13);
            this.Name = "ColorControl";
            this.Size = new System.Drawing.Size(370, 90);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel01;
        private System.Windows.Forms.Panel panel06;
        private System.Windows.Forms.Panel panel05;
        private System.Windows.Forms.Panel panel04;
        private System.Windows.Forms.Panel panel03;
        private System.Windows.Forms.Panel panel02;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel09;
        private System.Windows.Forms.Panel panel08;
        private System.Windows.Forms.Panel panel07;
        private System.Windows.Forms.TextBox textBoxBlue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxGreen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxRed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAlpha;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelCur;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}
