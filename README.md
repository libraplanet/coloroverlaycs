# ColorOverlayCs
## 概要
C# (WinForms) でカラー オーバーレイ。

## 環境
* Windows 10
* Visual Studio 20019 Community
* .NET Framewors 4.5
* C#
* WinForms


## sample

|&#x20;|&#x20;|
|:--|:--|
|![ss](https://bytebucket.org/libraplanet/coloroverlaycs/raw/58c767c48e65066a77fe951d1e1b3ee496740220/images/ss_00_00.png)|![ss](https://bytebucket.org/libraplanet/coloroverlaycs/raw/58c767c48e65066a77fe951d1e1b3ee496740220/images/ss_01_00.png)|
|![ss](https://bytebucket.org/libraplanet/coloroverlaycs/raw/58c767c48e65066a77fe951d1e1b3ee496740220/images/ss_00_01.png)|![ss](https://bytebucket.org/libraplanet/coloroverlaycs/raw/58c767c48e65066a77fe951d1e1b3ee496740220/images/ss_01_01.png)|
|![ss](https://bytebucket.org/libraplanet/coloroverlaycs/raw/58c767c48e65066a77fe951d1e1b3ee496740220/images/ss_00_02.png)|![ss](https://bytebucket.org/libraplanet/coloroverlaycs/raw/58c767c48e65066a77fe951d1e1b3ee496740220/images/ss_01_02.png)|
