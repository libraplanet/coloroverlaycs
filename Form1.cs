﻿using ColorOverlayCs.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace ColorOverlayCs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.SuspendLayout();
            this.ResumeLayout();
        }

        private void Message(string mes)
        {
            List<string> lineList;
            //recreate lines
            {
                List<string> list = new List<string>();
                DateTime dateTime = DateTime.Now;
                string text;
                if (mes == null)
                {
                    text = "";
                }
                else
                {
                    text = mes;
                }
                //if (mes != null)
                //{
                //    using (StringReader reader = new StringReader(mes))
                //    {
                //        string line;
                //        while ((line = reader.ReadLine()) != null)
                //        {
                //            list.Add(string.Format("[{0}] {1}",  new object[] {dateTime.ToString("yyyy/MM/dd HH:mm:ss.fff"),  line }));
                //        }
                //    }
                //}
#if false
                list.AddRange(textBoxMessage.Lines);
                list.Add(string.Format("[{0}] {1}", new object[] { dateTime.ToString("yyyy/MM/dd HH:mm:ss.fff"), text }));
                lineList = list.GetRange(Math.Max(list.Count -100, 0), Math.Min(list.Count, 100));
#else
                list.Add(string.Format("[{0}] {1}", new object[] { dateTime.ToString("yyyy/MM/dd HH:mm:ss.fff"), text }));
                list.AddRange(textBoxMessage.Lines);
                lineList = list.GetRange(0, Math.Min(list.Count, 100));
#endif
            }

            //set
            {
                StringBuilder sb = new StringBuilder();
                foreach (string line in lineList)
                {
                    sb.AppendLine(line);
                }
                textBoxMessage.Text = sb.ToString();
            }
        }

        private void UpdateDist()
        {
            UpdateDist(colorControlFg.CurrentColor);
        }

        private void UpdateDist(Color color)
        {
            Image imgOrg = pictureBoxOrg.Image;
            if(imgOrg == null)
            {
                pictureBoxDist.Image = null;
            }
            else
            {
                Bitmap bitmap = new Bitmap(imgOrg.Width, imgOrg.Height, PixelFormat.Format32bppArgb);
                using (Graphics g = Graphics.FromImage(bitmap))
                using (ImageAttributes attr = new ImageAttributes())
                {
                    float colR = color.R / 255.0f;
                    float colG = color.G / 255.0f;
                    float colB = color.B / 255.0f;
                    float colA = color.A / 255.0f;
                    attr.SetColorMatrix(new ColorMatrix(new float[][]{
                        new float[]{ 1.0f - colA,        0.0f,        0.0f, 0.0f, 0.0f },
                        new float[]{        0.0f, 1.0f - colA,        0.0f, 0.0f, 0.0f },
                        new float[]{        0.0f,        0.0f, 1.0f - colA, 0.0f, 0.0f },
                        new float[]{        0.0f,        0.0f,        0.0f, 1.0f, 0.0f },
                        new float[]{ colR * colA, colG * colA, colB * colA, 0.0f, 1.0f },
                    }));

                    g.DrawImage(imgOrg, new Rectangle(new Point(0, 0), bitmap.Size), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel, attr);
                }
                pictureBoxDist.Image = bitmap;
            }
        }

        private void buttonBrows_Click(object sender, EventArgs e)
        {
            if(openFileDialogImage.ShowDialog() == DialogResult.OK)
            {
                Message(string.Format("[Form1.buttonBrows_Click()] selected {0}", new object[] { openFileDialogImage.FileName }));
                try
                {
                    string filePath = openFileDialogImage.FileName;
                    using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        Image image = new Bitmap(stream);
                        pictureBoxOrg.Image = image;
                    }
                    textBoxImagePath.Text = filePath;
                    Message(string.Format("[Form1.buttonBrows_Click()] loaded."));
                    UpdateDist();
                    Message(string.Format("[Form1.buttonBrows_Click()] updated."));
                }
                catch (Exception ex)
                {
                    Message(ex.ToString());
                }
            }
            else
            {
                Message(string.Format("[Form1.buttonBrows_Click()] selected {0}", new object[] { openFileDialogImage.FileName }));
            }
        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            Message(string.Format("[Form1.buttonReload_Click()] reload {0}", new object[] { openFileDialogImage.FileName }));
            try
            {
                string filePath = openFileDialogImage.FileName;
                using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    Image image = new Bitmap(stream);
                    pictureBoxOrg.Image = image;
                    Message(string.Format("[Form1.buttonReload_Click()] reloaded."));
                    UpdateDist();
                    Message(string.Format("[Form1.buttonReload_Click()] updated."));
                }
                textBoxImagePath.Text = filePath;
            }
            catch (Exception ex)
            {
                Message(ex.ToString());
            }
        }

        private void timerUpdateDist_Tick(object sender, EventArgs e)
        {
            Message(string.Format("[Form1.timerUpdateDist_Tick()] start update."));
            UpdateDist();
            Message(string.Format("[Form1.timerUpdateDist_Tick()] updated."));
            (sender as Timer)?.Stop();
        }

        private void colorControlFg_CurrentColorChanged(object sender, EventArgs e)
        {
            Message(string.Format("[Form1.colorControlFg_CurrentColorChanged()] start update."));
            UpdateDist();
            Message(string.Format("[Form1.colorControlFg_CurrentColorChanged()] updated."));
        }

        private void colorControlBg_CurrentColorChanged(object sender, EventArgs e)
        {
            Message(string.Format("[Form1.colorControlFg_CurrentColorChanged()] start update."));
            Color color = colorControlBg.CurrentColor;
            pictureBoxOrg.BackColor = color;
            pictureBoxDist.BackColor = color;
            if(color.A == 0)
            {
                pictureBoxOrg.BackgroundImage = Resources.bg_transparent;
                pictureBoxDist.BackgroundImage = Resources.bg_transparent;
            }
            else
            {
                pictureBoxOrg.BackgroundImage = null;
                pictureBoxDist.BackgroundImage = null;
            }
            Message(string.Format("[Form1.colorControlFg_CurrentColorChanged()] updated."));
        }
    }
}
