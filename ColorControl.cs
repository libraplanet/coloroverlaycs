﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management.Instrumentation;

namespace ColorOverlayCs
{
    public partial class ColorControl : UserControl
    {
        public ColorDialog ColorDialog { get; set; } = new ColorDialog();
        public ColorModel ColorModel { get; private set; } = new ColorModel();

        public Color CurrentColor
        {
            get => ColorModel.Color;
            set => ColorModel.Color = value;
        }
        public event EventHandler CurrentColorChanged
        {
            add => panelCur.BackColorChanged += value;
            remove => panelCur.BackColorChanged -= value;
        }
        public Color Color01
        {
            get => panel01.BackColor;
            set => panel01.BackColor = value;
        }
        public Color Color02
        {
            get => panel02.BackColor;
            set => panel02.BackColor = value;
        }
        public Color Color03
        {
            get => panel03.BackColor;
            set => panel03.BackColor = value;
        }
        public Color Color04
        {
            get => panel04.BackColor;
            set => panel04.BackColor = value;
        }
        public Color Color05
        {
            get => panel05.BackColor;
            set => panel05.BackColor = value;
        }
        public Color Color06
        {
            get => panel06.BackColor;
            set => panel06.BackColor = value;
        }
        public Color Color07
        {
            get => panel07.BackColor;
            set => panel07.BackColor = value;
        }
        public Color Color08
        {
            get => panel08.BackColor;
            set => panel08.BackColor = value;
        }
        public Color Color09
        {
            get => panel09.BackColor;
            set => panel09.BackColor = value;
        }
        public Color Color10
        {
            get => panel10.BackColor;
            set => panel10.BackColor = value;
        }
        public Color Color11
        {
            get => panel11.BackColor;
            set => panel11.BackColor = value;
        }
        public Color Color12
        {
            get => panel12.BackColor;
            set => panel12.BackColor = value;
        }
        public Color Color13
        {
            get => panel13.BackColor;
            set => panel13.BackColor = value;
        }
        public Color Color14
        {
            get => panel14.BackColor;
            set => panel14.BackColor = value;
        }
        public Color Color15
        {
            get => panel15.BackColor;
            set => panel15.BackColor = value;
        }
        public Color Color16
        {
            get => panel16.BackColor;
            set => panel16.BackColor = value;
        }
        public Color Color17
        {
            get => panel17.BackColor;
            set => panel17.BackColor = value;
        }
        public Color Color18
        {
            get => panel18.BackColor;
            set => panel18.BackColor = value;
        }

        public ColorControl()
        {
            InitializeComponent();

            textBoxAlpha.DataBindings.Add("Text", ColorModel, "AlphaString", true, DataSourceUpdateMode.OnPropertyChanged);
            textBoxRed.DataBindings.Add("Text", ColorModel, "RedString", true, DataSourceUpdateMode.OnPropertyChanged);
            textBoxGreen.DataBindings.Add("Text", ColorModel, "GreenString", true, DataSourceUpdateMode.OnPropertyChanged);
            textBoxBlue.DataBindings.Add("Text", ColorModel, "BlueString", true, DataSourceUpdateMode.OnPropertyChanged);
            panelCur.DataBindings.Add("BackColor", ColorModel, "Color", true, DataSourceUpdateMode.OnPropertyChanged);

            textBoxAlpha.Tag = ColorModel;
            textBoxRed.Tag = ColorModel;
            textBoxGreen.Tag = ColorModel;
            textBoxBlue.Tag = ColorModel;
            panelCur.Tag = ColorModel;
        }

        private void panelCur_MouseClick(object sender, MouseEventArgs e)
        {
            Control controc = sender as Control;
            ColorModel model = controc?.Tag as ColorModel;
            if (model != null)
            {
                model.Color = Color.Transparent;
            }
        }

        private void panelCur_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Control controc = sender as Control;
            ColorModel model = controc?.Tag as ColorModel;
            if (model != null)
            {
                //color picker
                if (ColorDialog.ShowDialog() == DialogResult.OK)
                {
                    model.Color = ColorDialog.Color;
                }
            }
        }

        private void panel_MouseClick(object sender, MouseEventArgs e)
        {
            Panel panel = sender as Panel;
            MouseEventArgs mouse = e as MouseEventArgs;
            if (panel != null)
            {
                ColorModel.Color = panel.BackColor;
            }
        }

        private void panel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Panel panel = sender as Panel;
            if (panel != null)
            {
                if (ColorDialog.ShowDialog() == DialogResult.OK)
                {
                    panel.BackColor = ColorDialog.Color;
                    ColorModel.Color = ColorDialog.Color;
                }
            }
        }
    }
}
